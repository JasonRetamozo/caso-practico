package com.valtx.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.valtx.model.Producto;
import com.valtx.service.ProductoService;

@RestController
@RequestMapping("/api/producto")
public class ProductoREST {
	
	@Autowired
	private ProductoService productoService;
	
	@PostMapping ("/guardar")
	private ResponseEntity<Producto> guardar (@RequestBody Producto producto){
		Producto temporal = productoService.create(producto);
		
		try {
			return ResponseEntity.created(new URI("/api/producto"+temporal.getCod_producto())).body(temporal);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@GetMapping
	private ResponseEntity<List<Producto>> listar (){
		return ResponseEntity.ok(productoService.getAll());
	}
	
	@DeleteMapping
	private ResponseEntity<Void> eliminar (@RequestBody Producto producto){
		productoService.delete(producto);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping (value = "{id}")
	private ResponseEntity<Optional<Producto>> listarPorID (@PathVariable("id") String id){
		return ResponseEntity.ok(productoService.findById(id));
	}
	
	@PutMapping
	private ResponseEntity<Producto> actualizar (@RequestBody Producto producto){
		Optional<Producto> optional = productoService.findById(producto.getCod_producto());
		if(optional.isPresent()) {
			Producto updated = productoService.create(producto);
			return ResponseEntity.ok(updated);
		}else {
			return ResponseEntity.notFound().build();
		}
	}

}
