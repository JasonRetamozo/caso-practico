package com.valtx.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.valtx.model.Sucursal;
import com.valtx.service.SucursalService;

@RestController
@RequestMapping("/api/sucursal")
public class SucursalREST {

	@Autowired
	private SucursalService sucursalService;
	
	@PostMapping ("/guardar")
	private ResponseEntity<Sucursal> guardar (@RequestBody Sucursal sucursal){
		Sucursal temporal = sucursalService.create(sucursal);
		
		try {
			return ResponseEntity.created(new URI("/api/sucursal"+temporal.getCod_sucursal())).body(temporal);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@GetMapping
	private ResponseEntity<List<Sucursal>> listar (){
		return ResponseEntity.ok(sucursalService.getAll());
	}
	
	@DeleteMapping
	private ResponseEntity<Void> eliminar (@RequestBody Sucursal sucursal){
		sucursalService.delete(sucursal);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping (value = "{id}")
	private ResponseEntity<Optional<Sucursal>> listarPorID (@PathVariable("id") String id){
		return ResponseEntity.ok(sucursalService.findById(id));
	}
	
	@PutMapping
	private ResponseEntity<Sucursal> actualizar (@RequestBody Sucursal sucursal){
		Optional<Sucursal> optional = sucursalService.findById(sucursal.getCod_sucursal());
		if(optional.isPresent()) {
			Sucursal updated = sucursalService.create(sucursal);
			return ResponseEntity.ok(updated);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
	
}
