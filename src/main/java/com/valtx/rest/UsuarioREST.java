package com.valtx.rest;

import java.net.URI;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.valtx.model.Sucursal;
import com.valtx.model.Usuario;
import com.valtx.service.SucursalService;
import com.valtx.service.UsuarioService;

@RestController
@RequestMapping("/api/usuario")
public class UsuarioREST {

	@Autowired
	private UsuarioService usuarioService;
	
	@PostMapping ("/guardar")
	private ResponseEntity<Usuario> guardar (@RequestBody Usuario usuario){
		Usuario temporal = usuarioService.create(usuario);
		
		try {
			return ResponseEntity.created(new URI("/api/usuario"+temporal.getCod_usuario())).body(temporal);
		} catch (Exception e) {
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
		}
	}
	
	@GetMapping
	private ResponseEntity<List<Usuario>> listar (){
		return ResponseEntity.ok(usuarioService.getAll());
	}
	
	@DeleteMapping
	private ResponseEntity<Void> eliminar (@RequestBody Usuario usuario){
		usuarioService.delete(usuario);
		return ResponseEntity.ok().build();
	}
	
	@GetMapping (value = "{id}")
	private ResponseEntity<Optional<Usuario>> listarPorID (@PathVariable("id") String id){
		return ResponseEntity.ok(usuarioService.findById(id));
	}
	
	@PutMapping
	private ResponseEntity<Usuario> actualizar (@RequestBody Usuario usuario){
		Optional<Usuario> optional = usuarioService.findById(usuario.getCod_usuario());
		if(optional.isPresent()) {
			Usuario updated = usuarioService.create(usuario);
			return ResponseEntity.ok(updated);
		}else {
			return ResponseEntity.notFound().build();
		}
	}
}
