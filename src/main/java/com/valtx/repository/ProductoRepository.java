package com.valtx.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.valtx.model.Producto;

@Repository
public interface ProductoRepository extends JpaRepository<Producto, String>{

}
