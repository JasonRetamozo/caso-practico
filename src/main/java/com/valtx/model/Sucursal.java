package com.valtx.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "sucursal")
public class Sucursal {
	@Id
	private String cod_sucursal;
	private String nombre;
	
	public String getCod_sucursal() {
		return cod_sucursal;
	}
	public void setCod_sucursal(String cod_sucursal) {
		this.cod_sucursal = cod_sucursal;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public Sucursal() {
		super();
		// TODO Auto-generated constructor stub
	}
}
