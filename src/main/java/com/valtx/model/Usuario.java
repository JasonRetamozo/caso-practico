package com.valtx.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table (name = "usuario")
public class Usuario {

	@Id
	private String cod_usuario;
	private String nombre;
	private String userr;
	private String password;
	private String cod_sucursal;
	public String getCod_usuario() {
		return cod_usuario;
	}
	public void setCod_usuario(String cod_usuario) {
		this.cod_usuario = cod_usuario;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getUserr() {
		return userr;
	}
	public void setUserr(String userr) {
		this.userr = userr;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getCod_sucursal() {
		return cod_sucursal;
	}
	public void setCod_sucursal(String cod_sucursal) {
		this.cod_sucursal = cod_sucursal;
	}
	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
