package com.valtx.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.valtx.model.Producto;
import com.valtx.repository.ProductoRepository;

@Service
public class ProductoService {

	@Autowired
	private ProductoRepository productoRepository;
	
	public Producto create (Producto producto) {
		return productoRepository.save(producto);
	}
	public List<Producto> getAll () {
		return productoRepository.findAll();
	}
	public void delete (Producto producto) {
		productoRepository.delete(producto);
	}
	public Optional<Producto> findById (String id) {
		return productoRepository.findById(id);
	}
}
