package com.valtx.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.valtx.model.Usuario;
import com.valtx.repository.UsuarioRepository;

@Service
public class UsuarioService {
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public Usuario create (Usuario usuario) {
		return usuarioRepository.save(usuario);
	}
	public List<Usuario> getAll () {
		return usuarioRepository.findAll();
	}
	public void delete (Usuario usuario) {
		usuarioRepository.delete(usuario);
	}
	public Optional<Usuario> findById (String id) {
		return usuarioRepository.findById(id);
	}
}
