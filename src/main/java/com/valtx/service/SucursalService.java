package com.valtx.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.valtx.model.Sucursal;
import com.valtx.repository.SucursalRepository;

@Service
public class SucursalService {

	@Autowired
	private SucursalRepository sucursalRepository;
	
	public Sucursal create (Sucursal sucursal) {
		return sucursalRepository.save(sucursal);
	}
	public List<Sucursal> getAll () {
		return sucursalRepository.findAll();
	}
	public void delete (Sucursal sucursal) {
		sucursalRepository.delete(sucursal);
	}
	public Optional<Sucursal> findById (String id) {
		return sucursalRepository.findById(id);
	}
}
